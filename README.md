# DB2Eloquent

Generates Eloquent ORM Models based on your database tables.
Support only for Postgresql now!!!
## Usage

`bin/db2eloquent ./config.php`

## Minimum config setup

```php
return [
	'db' => [
		'driver'    => 'pgsql',
		'host'      => 'localhost',
		'database'  => 'database',
		'username'  => 'login',
		'password'  => 'password',
		'charset'   => 'utf8',
	],
	'namespace' => 'DB2Eloquent\\ORM',
	'path' => 'src/ORM',

	'filter' => [
		'include' => [
			'list' => [
				'shop.user',
				'shop.addresses'
			],
			'regex' => '/public\..*/'
		],
		'exclude' => [
			'list' => [
				'public.address'
			],
			'regex' => '/public\.pk_test.*/'
		],
	],
    
    'rules' => [
        'singular' => [
            'pattern' => 'value'
        ],

        'plural' => [
            'pattern' => 'value'
        ]   
    ],
];
```
