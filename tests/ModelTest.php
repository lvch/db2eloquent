<?php

use DB2Eloquent\Database\Capsule\Manager as Capsule;
use DB2Eloquent\ORM\Shop\Address;
use DB2Eloquent\ORM\Shop\User;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase {

	protected function setUp() {
		$config = require dirname(__DIR__) . '/config.php';

		$capsule = new Capsule();
		$capsule->addConnection($config['db']);
		$capsule->setAsGlobal();
		$capsule->bootEloquent();
	}

	private function isPhp7() {
		return phpversion() > 6;
	}

	public function testUpdate() {
		$address = Address::first();

		$address->name .= '_';
		$this->assertTrue($address->save());

		$address->name = substr($address->name, 0, -1);
		$this->assertTrue($address->save());
	}

	public function testLinks() {
		$user = User::find(1);

		$this->assertEquals($user->users[0]->id, 4);
		$this->assertNull($user->user);

		$user = User::find(4);
		$this->assertEquals($user->users->count(), 0);
		$this->assertEquals($user->user->id, 1);
	}
}
