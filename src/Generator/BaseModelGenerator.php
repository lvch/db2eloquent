<?php

namespace DB2Eloquent\Generator;

use DB2Eloquent\Command\GenerateModelsCommand;
use DB2Eloquent\Generator\RelationMethodGenerator;
use DB2Eloquent\Model;
use Illuminate\Support\Collection;
use Zend\Code\Generator\PropertyGenerator;
use Zend\Code\Generator\PropertyValueGenerator;
use Zend\Code\Generator\ValueGenerator;

class BaseModelGenerator extends ModelGenerator {

	const CONST_TABLE = 'TABLE';

	const OUTPUT_LINK_INFO = "\r\tL: <%s>%s %s</> (%s)";
	const OUTPUT_TAG_COMMENT = 'comment';
	const OUTPUT_TAG_INFO = 'info';
	const OUTPUT_TAG_ERROR = 'error';

	public function generate() {
		$this->initClass();
		$this->fillPK();
		$this->fillIncrementing();
		$this->fillTimestamp();
		$this->fillConstants();
		$this->fillRelations();

		return $this->getClass();
	}

	protected function initClass() {
		$this->getClass()
			->setNamespaceName(
				$this->modelInfo->getFullBaseNamespace()
			)
			->setName($this->modelInfo->getClassName())
			->addUse(Model::class)
			->setExtendedClass(Model::class)
			->addConstant(static::CONST_TABLE, $this->modelInfo->getFullTableName())
			->addProperty(
				'table',
				new PropertyValueGenerator(
					$this->modelInfo->getClassName() . '::' . static::CONST_TABLE,
					ValueGenerator::TYPE_CONSTANT
				),
				PropertyGenerator::FLAG_PROTECTED
			);

		$this->getOutput()->writeln(
			'M: <info>'
				. $this->modelInfo->getClassName() . '</info> ('
				. $this->modelInfo->getFullTableName() . ')'
		);
	}

	protected function fillPK() {
		$this->modelInfo->getPk()
			&& $this->getClass()->addProperty(
				'primaryKey',
				$this->modelInfo->getPk(),
				PropertyGenerator::FLAG_PROTECTED
			);
	}

	protected function fillIncrementing() {
		!$this->modelInfo->getIncrementing()
			&& $this->getClass()->addProperty(
				'incrementing',
				false
			);
	}

	protected function fillTimestamp() {
		$columns = $this->modelInfo->getColumns();

		if (
			!$columns->contains('created_at')
			|| !$columns->contains('updated_at')
		) {
			$this->getClass()->addProperty('timestamps', false);
		}
	}

	protected function fillConstants() {
		foreach ($this->modelInfo->getColumns() as $column) {
			$const = new PropertyGenerator(
				$column->getConstant(),
				new PropertyValueGenerator($column->getName()),
				PropertyGenerator::FLAG_CONSTANT
			);

			$column->getComment()
				&& $const->setDocBlock($column->getComment());

			$this->getClass()->addConstantFromGenerator($const);
		}

		$constProperties = $this->modelInfo->getProperties();
		foreach ($constProperties as $property) {
			$this->getClass()->addConstant(
				$property->getConstant(),
				$property->getName()
			);
		}
	}

	protected function fillRelations() {
		$this->getGroupedRelations()
			->map(function($relations) {
				if ($relations->count() > 1) {
					$relations->map(function($relation) {
						$relation->setUseFqn(true);
					});
				}

				$relations->map(function($relation) {
					$this->fillRelation($relation);
				});
			});
	}

	protected function fillRelation(RelationMethodGenerator $relation) {
		if ($method = $relation->generate()) {
			$methodName = $method->getName();
			if (!($conflict = $this->getClass()->hasMethod($methodName))) {
				$this->getClass()->addMethodFromGenerator($method);

				foreach ($relation->getUses() as $use) {
					$this->getClass()->addUse(...$use);
				}
			}

			$this->outputRelationInfo(
				$conflict ? static::OUTPUT_TAG_ERROR : static::OUTPUT_TAG_INFO,
				$method->getName(),
				$relation->__toString(),
				true
			);
		}
	}

	protected function outputRelationInfo(
		$tag,
		$methodName,
		$strRelation,
		$newline = false
	) {
		if ($this->context->getOutput()->isVerbose()) {
			$this->context->getOutput()->write(
				sprintf(
					static::OUTPUT_LINK_INFO,
					$tag,
					[
						static::OUTPUT_TAG_COMMENT => '.',
						static::OUTPUT_TAG_INFO => '+',
						static::OUTPUT_TAG_ERROR => '-',
					][$tag],
					$methodName,
					$strRelation
				),
				$newline
			);
		}
	}

	protected function getGroupedRelations() {
		$relations = new Collection();

		foreach ($this->modelInfo->getLinks() as $link) {
			$relations[] = new OneToManyMethodGenerator(
				$this->context,
				$this->modelInfo,
				$link
			);
		}

		foreach ($this->modelInfo->getManyToManyLinks() as $link) {
			$relations[] = new ManyToManyMethodGenerator(
				$this->context,
				$this->modelInfo,
				$link
			);
		}

		return $relations->groupBy(function($relation) {
			return $relation->getMethodName();
		});
	}

	protected function getOutput() {
		return $this->context->getOutput();
	}
}
