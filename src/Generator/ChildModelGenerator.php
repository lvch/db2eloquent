<?php

namespace DB2Eloquent\Generator;

use DB2Eloquent\Command\GenerateModelsCommand;

class ChildModelGenerator extends ModelGenerator {

	public function generate() {
		return $this->getClass()
			->setNamespaceName(
				$this->modelInfo->getFullNamespace()
			)
			->setName($this->modelInfo->getClassName())
			->addUse(
				$this->modelInfo->getFullBaseClassName(),
				$this->getBaseClassName()
			)
			->setExtendedClass($this->getBaseClassName());
	}

	private function getBaseClassName() {
		return GenerateModelsCommand::NAMESPACE_BASE
			. $this->modelInfo->getClassName();
	}
}
