<?php

namespace DB2Eloquent\Adapter;

use DB2Eloquent\Traits\LazyProperty;
use DB2Eloquent\Traits\SmartProperties;

abstract class Adapter {

	use SmartProperties;
	use LazyProperty;

	protected $data = null;

	public function __construct(array $data) {
		$this->data = $data;
	}

	protected function dataPropProxy($field, $key, $default = null) {
		return $this->lazyProp($field, function() use($key, $default) {
			return isset($this->data[$key])
				? $this->data[$key]
				: $default;
		});
	}
}
