<?php

namespace DB2Eloquent;

use DB2Eloquent\Interfaces\InterfaceContext;
use DB2Eloquent\Traits\StaticSinglton;
use Doctrine\Inflector\Inflector as DoctrineInflector;
use Doctrine\Inflector\Rules\English\Inflectible;
use Doctrine\Inflector\Rules\English\Rules;
use Doctrine\Inflector\Rules\English\Uninflected;
use Doctrine\Inflector\Rules\Pattern;
use Doctrine\Inflector\Rules\Patterns;
use Doctrine\Inflector\Rules\Ruleset;
use Doctrine\Inflector\Rules\Substitutions;
use Doctrine\Inflector\Rules\Transformation;
use Doctrine\Inflector\Rules\Transformations;
use Doctrine\Inflector\RulesetInflector;

class Inflector extends DoctrineInflector {

	use StaticSinglton;

	public function __construct(InterfaceContext $context) {
		if ($rules = $context->getConfig()->rules) {
			$singularizer = isset($rules['singular'])
				?  new RulesetInflector(
					new Ruleset (
						new Transformations(...(
							call_user_func(
								function () use ($rules) : iterable {
									foreach ($rules['singular'] as $pattern => $rule) {
										yield new Transformation(new Pattern($pattern), $rule);
									}

									yield from Inflectible::getSingular();
								}
							)
						)),
						new Patterns(...Uninflected::getSingular()),
						(new Substitutions(...Inflectible::getIrregular()))->getFlippedSubstitutions()
					)
				)
				: new RulesetInflector(
					Rules::getSingularRuleset()
				);

			$pluralizer = isset($rules['plural'])
				? new RulesetInflector(
					new Ruleset (
						new Transformations(...(
						call_user_func(
							function () use ($rules) : iterable {
								foreach ($rules['plural'] as $pattern => $rule) {
									yield new Transformation(new Pattern($pattern), $rule);
								}

								yield from Inflectible::getPlural();
							}
						)
						)),
						new Patterns(...Uninflected::getPlural()),
						new Substitutions(...Inflectible::getIrregular())
					)
				)
				: new RulesetInflector(
					Rules::getPluralRuleset()
				);
		} else {
			$singularizer = new RulesetInflector(
				Rules::getSingularRuleset()
			);

			$pluralizer = new RulesetInflector(
				Rules::getPluralRuleset()
			);
		}
		parent::__construct($singularizer, $pluralizer);
	}
}
