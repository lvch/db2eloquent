<?php

namespace DB2Eloquent\Traits;

trait StaticSinglton {

	protected static $_instances = [];

	public static function get() {
		if (!isset(static::$_instances[static::class])) {
			static::$_instances[static::class] = new static(...func_get_args());
		}

		return static::$_instances[static::class];
	}
}
