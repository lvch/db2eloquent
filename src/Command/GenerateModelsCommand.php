<?php

namespace DB2Eloquent\Command;

use DB2Eloquent\Adapter\ConfigAdapter;
use DB2Eloquent\DBInfo\Postgresql\ModelInfo;
use DB2Eloquent\Generator\BaseModelGenerator;
use DB2Eloquent\Generator\ChildModelGenerator;
use DB2Eloquent\Inflector;
use DB2Eloquent\Interfaces\InterfaceContext;
use DB2Eloquent\Traits\LazyProperty;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use \DB2Eloquent\Database\Capsule\Manager as Capsule;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zend\Code\Generator\FileGenerator;

class GenerateModelsCommand extends Command implements InterfaceContext {

	use LazyProperty;

	const NAMESPACE_BASE = 'Base';

	protected $input = null;
	protected $output = null;

	protected static $defaultName = 'app:generate-models';

	private $modelsList = null;
	private $config = null;

	private $basePathInfo = null;

	protected function configure() {
		$this
			->setDescription('Generates models based on postgresql tables.')
			->addArgument('config', InputArgument::REQUIRED);
	}

	public function getConfig() {
		return $this->config;
	}

	public function getModelsList() {
		return $this->lazyProp('modelsList', function() {
			$dbLists = [
				'pgsql' => \DB2Eloquent\DBInfo\Postgresql\ModelsList::class
			];

			$list = isset($dbLists[$this->config->db['driver']])
				? $dbLists[$this->config->db['driver']]
				: [];

			return $list::create($this);
		});
	}

	public function getInput() {
		return $this->input;
	}

	public function getOutput() {
		return $this->output;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->input = $input;
		$this->output = $output;

		$this->initCapsule($input);
		$this->generate();

		return 0;
	}

	protected function generate() {
		Inflector::get($this);
		$modelsIncluded = $this->getModelsList()
			->filter(function(ModelInfo $modelInfo) {
				return $modelInfo->isIncluded();
			});

		foreach ($modelsIncluded as $modelInfo) {
			$this->generateBaseModel($modelInfo);
			$this->generateModel($modelInfo);
		}
	}

	private function generateBaseModel(ModelInfo $modelInfo) {
		$classGenerator = (new BaseModelGenerator($this, $modelInfo))
			->generate();

		$fileGenerator = new FileGenerator([
			'classes' => [$classGenerator],
		]);

		file_put_contents(
			$this->getBaseModelDir($modelInfo)
				. '/' . $modelInfo->getClassName() . '.php',
			$fileGenerator->generate()
		);
	}

	private function generateModel(ModelInfo $modelInfo) {
		$filePath = $this->getModelDir($modelInfo)
			. '/' . $modelInfo->getClassName() . '.php';

		if (!file_exists($filePath)) {
			$classGenerator = (new ChildModelGenerator($this, $modelInfo))
				->generate();

			$fileGenerator = new FileGenerator([
				'classes' => [$classGenerator],
			]);

			file_put_contents(
				$filePath,
				$fileGenerator->generate()
			);
		}
	}

	private function initCapsule(InputInterface $input) {
		$configFileInfo = new \SplFileInfo($input->getArgument('config'));
		if (!$configFileInfo->isFile()) {
			throw new FileNotFoundException(
				"File not found: " . $configFileInfo->getPathname()
			);
		}

		$config = include_once $configFileInfo->getRealPath();
		if (!is_array($config)) {
			throw new InvalidArgumentException(
				'Configuration should be an array.'
			);
		}
		$this->config = new ConfigAdapter($config);

		if (!\Illuminate\Database\Eloquent\Model::getConnectionResolver()) {
			$capsule = new Capsule();
			$capsule->addConnection($this->config->db);
			$capsule->setAsGlobal();
			$capsule->bootEloquent();
		}
	}

	private function getBaseModelDir(ModelInfo $modelInfo) {
		$baseModelDir = $this->getModelDir($modelInfo)
			. '/' . static::NAMESPACE_BASE;

		$this->prepareDir($baseModelDir);

		return $baseModelDir;
	}

	private function getModelDir(ModelInfo $modelInfo) {
		$modelDir = $this->getBasePathInfo()->getPathname()
			. '/' . $modelInfo->getNamespace();

		$this->prepareDir($modelDir);

		return $modelDir;
	}

	private function prepareDir($dir) {
		if (!is_dir($dir)) {
			mkdir($dir, 0770, true);
		}
	}

	private function getBasePathInfo() {
		return $this->lazyProp('basePathInfo', function() {
			return new \SplFileInfo($this->config->path);
		});
	}
}
