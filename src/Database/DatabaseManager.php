<?php

namespace DB2Eloquent\Database;

use Illuminate\Database\DatabaseManager as IlluminateDatabaseManager;

class DatabaseManager extends IlluminateDatabaseManager {

	public function getFactory() {
		return $this->factory;
	}
}
